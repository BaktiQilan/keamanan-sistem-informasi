from django.shortcuts import render

from .models import Users
# Create your views here.

def index(request):
    return render(request, 'ujian_aplikasi_1174083/index_1174083.html')

def user_detail(request):
    user = Users.objects.all()
    context = {
        'User':user,
    }
    return render(request, 'ujian_aplikasi_1174083/users_1174083.html', context)
